import React, { Component } from 'react';
import './App.css';

import { BrowserRouter as Router, Route /*, Link */ } from 'react-router-dom';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

import PageRegistrationStatistics from './components/PageRegistrationStatistics';
import PageAutorizationStatistics from './components/PageAutorizationStatistics';
import PageDropOutUsers from './components/PageDropOutUsers';

class App extends Component {
	
	render() {
		return (
			<Router>
				<div className="App">
					{/* Раздел меню */}
					<Navbar collapseOnSelect fixedTop>
						<Navbar.Header>
							<Navbar.Brand>
								<a href="/">Mock статистики для Ru.Sale</a>
							</Navbar.Brand>
							<Navbar.Toggle />
						</Navbar.Header>
						<Navbar.Collapse>
							<Nav>
								<NavItem eventKey={1} href="/">
									Отчет по регистрациям
								</NavItem>
								<NavItem eventKey={2} href="/PageAutorizationStatistics">
									Отчет по авторизациям
								</NavItem>
								<NavItem eventKey={3} href="/PageDropOutUsers">
									Отчет по отвалившимся пользователям
								</NavItem>
							</Nav>
						</Navbar.Collapse>
					</Navbar>

					{/* Routes section */}
					<Route exact path="/" component={PageRegistrationStatistics} />
					<Route
						path="/PageAutorizationStatistics"
						component={PageAutorizationStatistics}
					/>
					<Route
						path="/PageDropOutUsers"
						component={PageDropOutUsers}
					/>
				</div>
			</Router>
		);
	}
}

export default App;
