const chartOptions = {
	cutoutPercentage: 70,
	animation: {
		duration: 1000,
		easing: 'easeInOutElastic',
	},
	layout: {
		padding: {
			left: 0,
			right: 0,
			top: 32,
			bottom: 48
		}
	},
	legend: {
		display: true,
		position: 'right',
		labels: {
			boxWidth: 24,
			padding: 16,
			usePointStyle: true
		}
	},
	scales: {
		yAxes: [
			{
				id: 'first-y-axis',
				type: 'linear',
				position: 'left',
				offset: true,
				gridLines: {
				},
				scaleLabel: {
					display: true,
					labelString: 'Количество пользователей'
				},
				ticks: {
					beginAtZero: true,
					stepSize: 5,
					maxTicksLimit: 11,
					callback: value => {
						if (Math.abs(value) < 1 && value !== 0) {
							return '';
						}
						if (parseInt(value, 10) !== value && value !== 0) {
							return '';
						}
						return value;
					}
				}
			}
		]
	},
	tooltips: {
		titleSpacing: 16,
		titleMarginBottom: 8,
		bodySpacing: 16,
		xPadding: 12,
		yPadding: 12,
		caretPadding: 12,
		borderWidth: 4
	},
	elements: {
		line: {
			tension: 0.5,
			fill: false
		},
		point: {
			radius: 2,
			borderWidth: 2,
			hitRadius: 6
		}
	},
	colors: ['#a4c639', 'rgb(88, 86, 214)', 'blue', 'orange', 'red', 'yellow', 'green', 'purple', 'teal', 'pink', 'magenta']
};

export default chartOptions;
