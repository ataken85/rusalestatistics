const chartFakeData = [
	{
		type: 'MONTH',
		dateView: {
			'2018-03-29': {
				kaluga: {
					android: 2,
					ios: 3,
					web: 0,
					winmobile: 1
				},
				ufa: {
					android: 4,
					ios: 0,
					web: 2,
					winmobile: 1
				}
			},
			'2018-03-28': {
				ufa: {
					android: 1,
					ios: 5,
					web: 3,
					winmobile: 0
				}
			},
			'2018-03-21': {
				kaluga: {
					android: 0,
					ios: 3,
					web: 2,
					winmobile: 0
				}
			},
			'2018-03-20': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 2,
					ios: 1,
					web: 5,
					winmobile: 1
				}
			},
			'2018-03-23': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 4,
					winmobile: 0
				}
			},
			'2018-04-10': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				}
			},
			'2018-03-22': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				}
			},
			'2018-04-11': {
				kaluga: {
					android: 2,
					ios: 1,
					web: 3,
					winmobile: 0
				}
			},
			'2018-03-25': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 3,
					winmobile: 0
				}
			},
			'2018-03-24': {
				ufa: {
					android: 1,
					ios: 1,
					web: 0,
					winmobile: 1
				},
				kaluga: {
					android: 5,
					ios: 1,
					web: 0,
					winmobile: 1
				}
			},
			'2018-03-27': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'2018-03-26': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				}
			},
			'2018-03-30': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				}
			},
			'2018-03-18': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 3,
					ios: 1,
					web: 2,
					winmobile: 0
				}
			},
			'2018-04-09': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 5
				},
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 5
				}
			},
			'2018-04-07': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'2018-03-19': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'2018-04-08': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				}
			},
			'2018-04-01': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'2018-03-31': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'2018-04-02': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				}
			},
			'2018-04-05': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'2018-04-06': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				}
			},
			'2018-04-03': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'2018-04-04': {
				ufa: {
					android: 1,
					ios: 2,
					web: 0,
					winmobile: 6
				},
				kaluga: {
					android: 0,
					ios: 1,
					web: 3,
					winmobile: 0
				}
			}
		}
	},
	{
		type: 'MONTH3',
		dateView: {
			'6 2018-02': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'5 2018-02': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'5 2018-01': {
				ufa: {
					android: 2,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'4 2018-01': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'7 2018-02': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'3 2018-01': {
				ufa: {
					android: 2,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'8 2018-02': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'14 2018-04': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'9 2018-03': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'9 2018-02': {
				ufa: {
					android: 2,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'13 2018-03': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'11 2018-03': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'12 2018-03': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'15 2018-04': {
				ufa: {
					android: 2,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'10 2018-03': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			}
		}
	},
	{
		type: 'HALF_YEAR',
		dateView: {
			'53 2017-12': {
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'6 2018-02': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'5 2018-02': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'52 2017-12': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'5 2018-01': {
				ufa: {
					android: 2,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'4 2018-01': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'7 2018-02': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'3 2018-01': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'8 2018-02': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'2 2018-01': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'9 2018-03': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'9 2018-02': {
				ufa: {
					android: 2,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'1 2018-01': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'11 2018-03': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'12 2018-03': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'43 2017-10': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'42 2017-10': {
				ufa: {
					android: 2,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'10 2018-03': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'46 2017-11': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			},
			'14 2018-04': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'44 2017-11': {
				ufa: {
					android: 2,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'44 2017-10': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'45 2017-11': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'13 2018-03': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'49 2017-12': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'48 2017-11': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'51 2017-12': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'48 2017-12': {
				ufa: {
					android: 1,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 1,
					web: 0,
					winmobile: 0
				}
			},
			'47 2017-11': {
				ufa: {
					android: 3,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 4,
					web: 0,
					winmobile: 0
				}
			},
			'15 2018-04': {
				ufa: {
					android: 2,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 2,
					web: 0,
					winmobile: 0
				}
			},
			'50 2017-12': {
				ufa: {
					android: 4,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 3,
					web: 0,
					winmobile: 0
				}
			}
		}
	},
	{
		type: 'YEAR',
		dateView: {
			'2018-04': {
				ufa: {
					android: 5,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 6,
					web: 0,
					winmobile: 0
				}
			},
			'2017-04': {
				ufa: {
					android: 7,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 6,
					web: 0,
					winmobile: 0
				}
			},
			'2017-05': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 16,
					web: 0,
					winmobile: 0
				}
			},
			'2017-06': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 15,
					web: 0,
					winmobile: 0
				}
			},
			'2017-10': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 16,
					web: 0,
					winmobile: 0
				}
			},
			'2017-11': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 15,
					web: 0,
					winmobile: 0
				}
			},
			'2018-01': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 16,
					web: 0,
					winmobile: 0
				}
			},
			'2017-12': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 16,
					web: 0,
					winmobile: 0
				}
			},
			'2018-02': {
				ufa: {
					android: 14,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 14,
					web: 0,
					winmobile: 0
				}
			},
			'2018-03': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 16,
					web: 0,
					winmobile: 0
				}
			},
			'2017-07': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 16,
					web: 0,
					winmobile: 0
				}
			},
			'2017-08': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 16,
					web: 0,
					winmobile: 0
				}
			},
			'2017-09': {
				ufa: {
					android: 15,
					ios: 0,
					web: 0,
					winmobile: 0
				},
				kaluga: {
					android: 0,
					ios: 15,
					web: 0,
					winmobile: 0
				}
			}
		}
	}
];

export default chartFakeData;
