import _ from 'lodash';

function getFiltersData(data) {
	const cities = new Set();
	const devices = new Set();
	const searchScope = _(Object.values(data))
		.map(item => {
			return Object.values(item.dateView);
		})
		.flatten()
		.value();
	const searchScopeCities = _(searchScope)
		.map(key => {
			return Object.keys(key);
		})
		.flatten()
		.value();
	const searchScopeDevices = _(searchScope)
		.map(key => {
			return Object.values(key);
		})
		.flatten()
		.map(key => {
			return Object.keys(key);
		})
		.flatten()
		.value();
	searchScopeCities.forEach(city => {
		cities.add(city);
	});
	searchScopeDevices.forEach(device => {
		devices.add(device);
	});

	const selectCities = [
		...[...cities].map(city => {
			return { value: city, label: city };
		})
	];

	return ({selectCities, devices: [...devices]});
}

export default getFiltersData;
