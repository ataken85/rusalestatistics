import axios from 'axios';

const axiosInstance = axios.create({
	baseURL: 'https://rusale-statistics-dev.introa.ru/rest/v1/'
});

export default axiosInstance;
