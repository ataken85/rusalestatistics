import _ from 'lodash';
import chartOptions from './chartOptions';

function chartsCalculate(data, activeCities, devices) {
	let day = [];
	let week = [];
	let month = [];
	let treeMonth = [];
	let sixMonth = [];
	let year = [];
	_.map(data, item => {
		if (item.type && item.type === 'DAY') {
			day = item.dateView;
		}
		if (item.type && item.type === 'WEEK') {
			week = item.dateView;
		}
		if (item.type && item.type === 'MONTH') {
			month = item.dateView;
		}
		if (item.type && item.type === 'MONTH3') {
			treeMonth = item.dateView;
		}
		if (item.type && item.type === 'HALF_YEAR') {
			sixMonth = item.dateView;
		}
		if (item.type && item.type === 'YEAR') {
			year = item.dateView;
		}
		return (day, week, month, treeMonth, sixMonth, year);
	});
	
	return {
		day: bakeData(day, activeCities, devices),
		week: bakeData(week, activeCities, devices),
		month: bakeData(month, activeCities, devices),
		treeMonth: bakeData(treeMonth, activeCities, devices),
		sixMonth: bakeData(sixMonth, activeCities, devices),
		year: bakeData(year, activeCities, devices)
	};
}

function bakeData(thatData, activeCities, devices) {
	let labels = [];

	const activeCitiesFlat = _(activeCities)
		.map(city => {
			return city.value;
		})
		.value();

	if (devices.length > 0 && _.isPlainObject(devices[0])) {
		devices = _(devices)
			.map(device => {
				return device.value;
			})
			.value();
	}

	const completeData = [];
	devices.forEach((device, index) => {
		completeData.push({
			label: device,
			borderColor: chartOptions.colors[index],
			fill: false,
			data: []
		});
	});

	// Сортировка по дате
	const sortByKeys = object => {
		const keys = Object.keys(object);
		const sortedKeys = _.sortBy(keys);

		return _.fromPairs(_.map(sortedKeys, key => [key, object[key]]));
	};
	thatData = sortByKeys(thatData);

	// Набиваем значения
	_(thatData).each((item, key) => {
		labels.push(key);
		item = _.pick(item, activeCitiesFlat);
		completeData.forEach(finalItem => {
			finalItem.data.push(_.sumBy(Object.values(item), finalItem.label));
		});

		return item;
	});

	const dataset = {
		labels,
		datasets: completeData
	};

	// this.setState({ [kName]: dataset });

	return dataset;
}

export default chartsCalculate;
