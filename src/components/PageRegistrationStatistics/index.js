import React, { Component } from 'react';
import { PageHeader, Grid, Row, Col } from 'react-bootstrap';
import { Line } from 'react-chartjs-2';
import Loader from 'react-loader-spinner';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

import axiosInstance from '../../utilModules/axiosInstance';
import chartOptions from '../../utilModules/chartOptions';
import getFiltersData from '../../utilModules/getFiltersData';
import chartsCalculate from '../../utilModules/chartsCalculate';

class PageRegistrationStatistics extends Component {
	constructor(props) {
		super(props);

		this.calcData = this.calcData.bind(this);
		this.getAllCities = this.getAllCities.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleInputChangeDevices = this.handleInputChangeDevices.bind(this);

		this.state = {
			data: [],
			cities: [],
			activeCities: [],
			devices: [],
			activeDevices: [],
			month: [],
			treeMonth: [],
			sixMonth: [],
			year: []
		};
	}

	componentDidMount() {
		axiosInstance
			.get('/registration/newRegistrations')
			.then(res => {
				const { data } = res;
				this.setState({ data });
			})
			.catch(err => {
				console.info(err.message); // eslint-disable-line
			});
	}

	shouldComponentUpdate(nextProps, nextState) {
		const { activeCities, data, activeDevices } = this.state;
		if (activeCities !== nextState.activeCities  || activeDevices !== nextState.activeDevices) {
			this.calcData(nextState.data, nextState.activeCities, nextState.activeDevices);
			return true;
		}
		if (data !== nextState.data) {
			this.getAllCities(nextState.data);
			return true;
		}
		return true;
	}

	// Чиста сбор городов
	getAllCities(data) {
		const { selectCities, devices } = getFiltersData(data);
		const superDevices = [...devices.map((item) => { return { label: item, value: item }; })];
		this.setState({
			cities: selectCities,
			activeCities: selectCities,
			devices: superDevices,
			activeDevices: superDevices
		});

		this.calcData(data, selectCities, devices);
	}

	calcData(data, activeCities, devices) {
		const { month, treeMonth, sixMonth, year } = chartsCalculate(
			data,
			activeCities,
			devices
		);
		this.setState({ month, treeMonth, sixMonth, year });
	}

	handleInputChange(e) {
		this.setState({ activeCities: e });
	}

	handleInputChangeDevices(e) {
		this.setState({ activeDevices: e });
	}

	render() {
		const {
			month,
			treeMonth,
			sixMonth,
			year,
			cities,
			activeCities,
			devices,
			activeDevices
		} = this.state;

		const isLoading =
			month.length === 0 ||
			treeMonth.length === 0 ||
			sixMonth.length === 0 ||
			year.length === 0 ||
			devices.length === 0;

		if (isLoading) {
			return (
				<div className="loaderBackground">
					<Loader type="Audio" color="#fff" height="100" width="100" />
				</div>
			);
		}

		return (
			<div style={{ paddingTop: '96px' }}>
				<Grid>
					<Row className="show-grid fixedMenu">
						<Col xs={12} md={4} mdOffset={2}>
							<Select
								placeholder="Выберите город(а)"
								noResultsText="Ни одного совпадения имени города"
								clearAllText="Очистить выбранные города"
								clearValueText="Снять выбор города"
								value={activeCities}
								onChange={this.handleInputChange}
								options={cities}
								multi
							/>
						</Col>
						<Col xs={12} md={4}>
							<Select
								placeholder="Выберите устройство(а)"
								noResultsText="Ни одного совпадения названия устройства"
								clearAllText="Очистить выбранные устройства"
								clearValueText="Снять выбор устройства"
								value={activeDevices}
								onChange={this.handleInputChangeDevices}
								options={devices}
								multi
							/>
						</Col>
					</Row>
				</Grid>

				<PageHeader>Отчет по регистрациям</PageHeader>
				<Grid>
					{/* Графики */}
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h3>Данные за месяц</h3>
						</Col>
						<Col xs={12} md={12}>
							<Line data={month} options={chartOptions} />
						</Col>
					</Row>
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h3>Данные за три месяца</h3>
						</Col>
						<Col xs={12} md={12}>
							<Line data={treeMonth} options={chartOptions} />
						</Col>
					</Row>
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h3>Данные пол года</h3>
						</Col>
						<Col xs={12} md={12}>
							<Line data={sixMonth} options={chartOptions} />
						</Col>
					</Row>
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h3>Данные за год</h3>
						</Col>
						<Col xs={12} md={12}>
							<Line data={year} options={chartOptions} />
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

export default PageRegistrationStatistics;
