import React, { Component } from 'react';
import { PageHeader, Grid, Row, Col, Table, Pagination } from 'react-bootstrap';
import Loader from 'react-loader-spinner';
import 'react-select/dist/react-select.css';

import _ from 'lodash';

import axiosInstance from '../../utilModules/axiosInstance';

class PageDropOutUsers extends Component {
	constructor() {
		super();

		this.getSimpleDate = this.getSimpleDate.bind(this);
		this.loadData = this.loadData.bind(this);

		this.state = {
			data: [],
			pages: 7,
			currPage: 0,
		};
	}

	componentWillMount() {
		this.loadData();
	}

	loadData(e) {
		const page = e ? e.target.dataset.page : 1;
		this.setState({ currPage: (page - 1)});
		axiosInstance
			.get(`/authorization/miss/30/${page}`)
			.then(res => {
				const { data } = res;
				this.setState({ data });
			})
			.catch(err => {
				console.info(err.message); // eslint-disable-line
			});
	}

	getSimpleDate(date) {
		if (!_.isNull(date)) {
			const finalDate = new Date(date);
			return `${finalDate.getDate() < 10 ? '0' : ''
			}${finalDate.getDate()}.${
				finalDate.getMonth() < 9 ? '0' : ''
			}${finalDate.getMonth() + 1}.${finalDate.getFullYear()}`;
		}
		return '';
	}

	render() {
		const { data, pages, currPage } = this.state;

		const isLoading = _.isEmpty(data);

		const paginator = [];
		for (var i = 1; i <= pages; i++) {
			paginator.push(<Pagination.Item key={i} onClick={this.loadData} data-page={i}>{i}</Pagination.Item>);
		}

		if (isLoading) {
			return (
				<div className="loaderBackground">
					<Loader type="Audio" color="#fff" height="100" width="100" />
				</div>
			);
		}

		return (
			<div style={{ paddingTop: '92px' }}>
				<Grid>
					<Row className="show-grid fixedMenu">
						<Col xs={12} md={8} mdOffset={2}>
							{/*
							<Select
								placeholder="Выберите город(а)"
								noResultsText="Ни одного совпадения имени города"
								clearAllText="Очистить выбранные города"
								clearValueText="Снять выбор города"
								value={activeCities}
								onChange={this.handleInputChange}
								options={cities}
								multi
              />
              */}
						</Col>
					</Row>
				</Grid>
				<PageHeader>Отчет по отвалившимся пользователям</PageHeader>
				<Grid>
					{/* Графики */}
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<Table responsive hover striped>
								<thead>
									<tr>
										<th>#</th>
										<th>ID</th>
										<th>Юзер</th>
										<th>Город</th>
										<th>Девайс</th>
										<th>Email</th>
										<th>Телефон</th>
										<th>Дата</th>
									</tr>
								</thead>
								<tbody>
									{data.map((item, index) => {
										return (
											<tr key={index}>
												<td>{index + 1 + currPage * 30}</td>
												<td>{item.userId}</td>
												<td>{item.name}</td>
												<td>{item.city}</td>
												<td>{item.deviceId}</td>
												<td>{item.email}</td>
												<td>{item.phone}</td>
												<td>{this.getSimpleDate(item.date)}</td>
											</tr>
										);
									})}
								</tbody>
							</Table>
						</Col>
						<Col xs={12} md={12}>
							<Pagination bsSize="large">
								{ paginator	}
							</Pagination>
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

export default PageDropOutUsers;
