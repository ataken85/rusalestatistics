import React, { Component } from 'react';
import { PageHeader, Grid, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import Loader from 'react-loader-spinner';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { Line, Pie } from 'react-chartjs-2';
import DatePicker from 'react-date-picker';

import _ from 'lodash';

import axiosInstance from '../../utilModules/axiosInstance';
import chartOptions from '../../utilModules/chartOptions';
import getFiltersData from '../../utilModules/getFiltersData';
import chartsCalculate from '../../utilModules/chartsCalculate';

class PageAutorizationStatistics extends Component {
	constructor() {
		super();

		this.calcData = this.calcData.bind(this);
		this.getAllCities = this.getAllCities.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleInputChangeDevices = this.handleInputChangeDevices.bind(this);
		this.bakeMeAPie = this.bakeMeAPie.bind(this);
		this.getUsage = this.getUsage.bind(this);
		this.getRepeated = this.getRepeated.bind(this);
		this.getUniq = this.getUniq.bind(this);
		this.onChangeFrom = this.onChangeFrom.bind(this);
		this.onChangeTo = this.onChangeTo.bind(this);
		this.getSimpleDate = this.getSimpleDate.bind(this);
		this.getMyData = this.getMyData.bind(this);

		this.state = {
			data: [],
			dataUniq: [],
			dataRepeated: [],
			dataCalculated: {},
			dataUniqCalculated: {},
			dataRepeatedCalculated: {},
			cities: [],
			activeCities: [],
			devices: [],
			activeDevices: [],
			regDateFrom: new Date(new Date() - 31536000000),
			regDateTo: new Date()
		};
	}

	componentWillMount() {
		const { regDateFrom, regDateTo } = this.state;
		this.getMyData(regDateFrom, regDateTo);
	}

	shouldComponentUpdate(nextProps, nextState) {
		const {
			activeCities,
			activeDevices,
			regDateFrom,
			regDateTo,
			data,
			dataUniq,
			dataRepeated
		} = this.state;
		if (
			activeCities !== nextState.activeCities ||
			activeDevices !== nextState.activeDevices ||
			data !== nextState.data ||
			dataUniq !== nextState.dataUniq ||
			dataRepeated !== nextState.dataRepeated
		) {
			this.calcData(
				nextState.data,
				nextState.activeCities,
				nextState.activeDevices
			);
			this.calcData(
				nextState.dataUniq,
				nextState.activeCities,
				nextState.activeDevices,
				'dataUniqCalculated'
			);
			this.calcData(
				nextState.dataRepeated,
				nextState.activeCities,
				nextState.activeDevices,
				'dataRepeatedCalculated'
			);

			return true;
		}

		if (
			regDateFrom !== nextState.regDateFrom ||
			regDateTo !== nextState.regDateTo
		) {
			this.getMyData(nextState.regDateFrom, nextState.regDateTo);

			return true;
		}

		return true;
	}

	getUsage(regDateFrom, regDateTo) {
		return axiosInstance.get(
			`authorization/usage?regDateFrom=${this.getSimpleDate(
				regDateFrom
			)}&regDateTo=${this.getSimpleDate(regDateTo)}`
		);
	}
	getRepeated(regDateFrom, regDateTo) {
		return axiosInstance.get(
			`authorization/repeated?regDateFrom=${this.getSimpleDate(
				regDateFrom
			)}&regDateTo=${this.getSimpleDate(regDateTo)}`
		);
	}
	getUniq(regDateFrom, regDateTo) {
		return axiosInstance.get(
			`authorization/uniq?regDateFrom=${this.getSimpleDate(
				regDateFrom
			)}&regDateTo=${this.getSimpleDate(regDateTo)}`
		);
	}

	getMyData(regDateFrom, regDateTo) {
		axios
			.all([
				this.getUsage(regDateFrom, regDateTo),
				this.getRepeated(regDateFrom, regDateTo),
				this.getUniq(regDateFrom, regDateTo)
			])
			.then(
				axios.spread((data, dataRepeated, dataUniq) => {
					this.setState({
						data: [...data.data],
						dataRepeated: [...dataRepeated.data],
						dataUniq: [...dataUniq.data]
					});
					this.getAllCities([...data.data]);
				})
			);
	}

	// Чиста сбор городов
	getAllCities(data) {
		const { selectCities, devices } = getFiltersData(data);
		const superDevices = [
			...devices.map(item => {
				return { label: item, value: item };
			})
		];
		this.setState({
			cities: selectCities,
			activeCities: selectCities,
			devices: superDevices,
			activeDevices: superDevices
		});

		// this.calcData(data, selectCities, devices);
	}

	calcData(data, activeCities, activeDevices, type = 'dataCalculated') {
		const { day, week, month, treeMonth, sixMonth, year } = chartsCalculate(
			data,
			activeCities,
			activeDevices
		);
		this.setState({ [type]: { day, week, month, treeMonth, sixMonth, year } });
	}

	handleInputChange(e) {
		this.setState({ activeCities: e });
	}

	handleInputChangeDevices(e) {
		this.setState({ activeDevices: e });
	}

	bakeMeAPie(data) {
		if (!_.isEmpty(data)) {
			const pieLabel = [
				...data.datasets.map(item => {
					return item.label;
				})
			];
			const pieData = [
				...data.datasets.map(item => {
					return _.sum(item.data);
				})
			];

			return {
				datasets: [
					{
						data: pieData,
						backgroundColor: chartOptions.colors
					}
				],
				labels: pieLabel
			};
		}

		return {};
	}

	onChangeFrom(regDateFrom) {
		this.setState({ regDateFrom });
	}

	onChangeTo(regDateTo) {
		this.setState({ regDateTo });
	}

	getSimpleDate(date) {
		if (!_.isNull(date)) {
			return `${date.getFullYear()}-${
				date.getMonth() < 9 ? '0' : ''
			}${date.getMonth() + 1}-${date.getDate() < 10 ? '0' : ''}${date.getDate()}`;
		}
		return '';
	}

	render() {
		const {
			cities,
			activeCities,
			devices,
			activeDevices,
			data,
			dataUniq,
			dataRepeated,
			dataCalculated,
			dataUniqCalculated,
			dataRepeatedCalculated,
			regDateFrom,
			regDateTo
		} = this.state;

		const isLoading =
			devices.length === 0 ||
			_.isEmpty(data) ||
			_.isEmpty(dataUniq) ||
			_.isEmpty(dataRepeated);

		if (isLoading) {
			return (
				<div className="loaderBackground">
					<Loader type="Audio" color="#fff" height="100" width="100" />
				</div>
			);
		}

		return (
			<div style={{ paddingTop: '128px' }}>
				<Grid>
					<Row className="show-grid fixedMenu">
						<Col xs={12} md={4} mdOffset={2}>
							<Select
								placeholder="Выберите город(а)"
								noResultsText="Ни одного совпадения имени города"
								clearAllText="Очистить выбранные города"
								clearValueText="Снять выбор города"
								value={activeCities}
								onChange={this.handleInputChange}
								options={cities}
								multi
							/>
						</Col>
						<Col xs={12} md={4}>
							<Select
								placeholder="Выберите устройство(а)"
								noResultsText="Ни одного совпадения названия устройства"
								clearAllText="Очистить выбранные устройства"
								clearValueText="Снять выбор устройства"
								value={activeDevices}
								onChange={this.handleInputChangeDevices}
								options={devices}
								multi
							/>
						</Col>
						<Col xs={12} md={8} mdOffset={2}>
							<div style={{ height: '16px' }}></div>
							<span style={{ margin: '12px 8px 12px 24px' }}>Дата от:</span>
							<DatePicker onChange={this.onChangeFrom} value={regDateFrom} minDate={new Date('01-01-1999')} />
							<span style={{ margin: '12px 8px 12px 24px' }}>Дата до:</span>
							<DatePicker onChange={this.onChangeTo} value={regDateTo} />
						</Col>
					</Row>
				</Grid>
				<PageHeader>Отчет по использованию приложения</PageHeader>
				<Grid>
					{/* Графики */}
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h1>Количество авторизаций уникальных пользователей</h1>
						</Col>
					</Row>
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h3>Данные за месяц</h3>
						</Col>
						<Col xs={12} md={12}>
							<Line data={dataUniqCalculated.month} options={chartOptions} />
						</Col>
					</Row>
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h3>Данные за три месяца</h3>
						</Col>
						<Col xs={12} md={12}>
							<Line
								data={dataUniqCalculated.treeMonth}
								options={chartOptions}
							/>
						</Col>
					</Row>
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h3>Данные за пол года</h3>
						</Col>
						<Col xs={12} md={12}>
							<Line data={dataUniqCalculated.sixMonth} options={chartOptions} />
						</Col>
					</Row>
					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h3>Данные за год</h3>
						</Col>
						<Col xs={12} md={12}>
							<Line data={dataUniqCalculated.year} options={chartOptions} />
						</Col>
					</Row>

					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h1>Количество авторизаций пользователей</h1>
						</Col>
						<Col xs={12} md={6}>
							<h3>Данные за день</h3>
							<Pie data={this.bakeMeAPie(dataCalculated.day)} />
							<p />
						</Col>
						<Col xs={12} md={6}>
							<h3>Данные за неделю</h3>
							<Pie data={this.bakeMeAPie(dataCalculated.week)} />
							<p />
						</Col>
						<Col xs={12} md={6}>
							<h3>Данные за месяц</h3>
							<Pie data={this.bakeMeAPie(dataCalculated.month)} />
							<p />
						</Col>
						<Col xs={12} md={6}>
							<h3>Данные за три месяца</h3>
							<Pie data={this.bakeMeAPie(dataCalculated.treeMonth)} />
							<p />
						</Col>
					</Row>

					<Row className="show-grid">
						<Col xs={12} md={12}>
							<h1>
								Количество пользователей/повторных авторизаций пользователей
							</h1>
						</Col>
						<Col xs={12} md={6}>
							<h3>Данные за день</h3>
							<Pie data={this.bakeMeAPie(dataRepeatedCalculated.day)} />
							<p />
						</Col>
						<Col xs={12} md={6}>
							<h3>Данные за неделю</h3>
							<Pie data={this.bakeMeAPie(dataRepeatedCalculated.week)} />
							<p />
						</Col>
						<Col xs={12} md={6}>
							<h3>Данные за месяц</h3>
							<Pie data={this.bakeMeAPie(dataRepeatedCalculated.month)} />
							<p />
						</Col>
						<Col xs={12} md={6}>
							<h3>Данные за три месяца</h3>
							<Pie data={this.bakeMeAPie(dataRepeatedCalculated.treeMonth)} />
							<p />
						</Col>
					</Row>
					<Row>
						<Col xs={12} md={6}>
							<p style={{ height: '32px' }} />
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

export default PageAutorizationStatistics;
